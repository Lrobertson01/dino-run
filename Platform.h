#pragma once
#include "SpriteObject.h"
class Platform
	:public SpriteObject
{
public:
	Platform(sf::Vector2f position);
	void setPosition(sf::Vector2f newPosition);
	float getWidth();
};

