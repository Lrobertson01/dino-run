#include "Cactus.h"

Cactus::Cactus(sf::Vector2u Screensize)
	:Enemy("Assets/cactus.png", 50, 100, 1)
{
	sf::Vector2f newPosition;
	newPosition.x = ((float)Screensize.x - sprite.getGlobalBounds().width);
	newPosition.y = (((float)Screensize.y - sprite.getGlobalBounds().height) / 2.0f) + 5;
	sprite.setPosition(newPosition);
}
