#pragma once
#include <SFML/Graphics.hpp>
#include "LevelClass.h"

class Game
{
public:
	Game();
	void runGameLoop();

	void Input();
	void Update();
	void Draw();
	sf::RenderWindow& windowGet();

private:
	sf::RenderWindow Window;
	sf::Clock gameClock;
	LevelClass LevelScreenInstance;
	sf::Text exampleText;
};

