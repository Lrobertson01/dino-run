#include "Platform.h"
#include "AssetManager.h"

Platform::Platform(sf::Vector2f position)
	:SpriteObject(AssetManager::RequestTexture("Assets/Platform.png"))
{
	sprite.setPosition(position);

}



void Platform::setPosition(sf::Vector2f newPosition)
{
	sprite.setPosition(newPosition);
}

float Platform::getWidth()
{
	return float(sprite.getGlobalBounds().width);
}


