#include "LevelClass.h"
#include "Game.h"
#include <stdlib.h>
#include <time.h>
#include "AssetManager.h"

LevelClass::LevelClass(Game* newGamePointer)
    : playerInstance(newGamePointer->windowGet().getSize())
    , gamePointer(newGamePointer)
    , camera(newGamePointer->windowGet().getDefaultView())
    , obstacleBuffer(0)
    , ScrollSpeed(1)
    , ScrollSpeedIncrease(0.02)
    , Score(0)
    , newFont(AssetManager::RequestFont("Assets/enter-command.ttf"))
    , JumpSound(AssetManager::RequestSound("Assets/jump.wav"))
    , DeathSound(AssetManager::RequestSound("Assets/death.wav"))
    , BGM()
    , gameOverIcon(AssetManager::RequestTexture("Assets/button-quit.png"), sf::Vector2f(gamePointer->windowGet().getSize().x / 2, gamePointer->windowGet().getSize().y / 3))
    
{
    //Setup the platform position variable, that will set up where the floor will be built
    sf::Vector2f platformPosition (0 - 30, newGamePointer->windowGet().getSize().y/2 + 50 );
    //Cover the screen from left to right in platforms (function as floors)
    while (platformPosition.x <= newGamePointer->windowGet().getSize().x)
    {
        Platform newPlatform(platformPosition);
        platformPosition.x += newPlatform.getWidth();
        platformInstances.push_back(newPlatform);
    }

    for (int i = 0; i < 6; ++i)
    {
        starList.push_back(Stars(AssetManager::RequestTexture("Assets/star.png") , gamePointer->windowGet().getSize()));
    }


    //Setup the score text
    scoreText.setFont(newFont);
    scoreText.setString("Score: 0");
    scoreText.setCharacterSize(32);
    scoreText.setFillColor(sf::Color::White);
    scoreText.setPosition(gamePointer->windowGet().getSize().x - scoreText.getLocalBounds().width - 20, 40);

    gameOverText.setFont(newFont);
    gameOverText.setString("GAME OVER");
    gameOverText.setCharacterSize(32);
    gameOverText.setFillColor(sf::Color::White);
    gameOverText.setPosition(gamePointer->windowGet().getSize().x / 2 - 15, gamePointer->windowGet().getSize().y / 3 + 60);
    //Set our sounds
    
    BGM.openFromFile("Assets/music.ogg");
    BGM.play();
}

void LevelClass::input()
{
    if (playerInstance.getAlive())
    {
        playerInstance.Input(JumpSound);
    }
    
}

void LevelClass::update(sf::Time GameTime)
{
    if (playerInstance.getAlive())
    {
        //Set the scoreText to the score of this frame
        scoreText.setString("Score: " + std::to_string((int)Score));
        scoreText.setPosition(gamePointer->windowGet().getSize().x - scoreText.getLocalBounds().width - 20, 60);  //Set the score to increment itself based on how much you have
        //Check if its time to spawn new obstacles, if so add a new one
        obstacleBuffer += GameTime.asSeconds();
        addObstacle();
        //Set the player so that they automatically arent colliding with anything
        playerInstance.setColliding(false);
        for (int i = 0; i < platformInstances.size(); i++)
        {
            if (playerInstance.checkCollision(platformInstances[i].getHitbox()))
            {
                //Set that the player is on solid ground to true
                playerInstance.setColliding(true);
                //Set the loop to automatically end once we've found collision
                i = platformInstances.size();
            }
        }
        //Update every enemy/obstacle, this uses polymorphism as the enemy class makes up both cactus and pterodactyls
        for (int i = 0; i < EnemyList.size(); i++)
        {
            EnemyList[i].update(GameTime, ScrollSpeed);
            if (playerInstance.getHitbox().intersects(EnemyList[i].getHitbox())) //Run collision
            {
                playerInstance.setAlive(false);
                DeathSound.play();
            }
        }

        for (int i = 0; i < starList.size(); i++)
        {
            starList[i].update(GameTime);
        }

        playerInstance.Update(GameTime);
    }

}

void LevelClass::drawTo(sf::RenderTarget& Target)
{    
    if (playerInstance.getAlive())
    {
        for (int i = 0; i < EnemyList.size(); i++)
        {
            EnemyList[i].DrawTo(Target);
        }
        playerInstance.DrawTo(Target);
    }
    else
    {
        gameOverIcon.DrawTo(Target);
        gamePointer->windowGet().draw(gameOverText);
    }

    for (int i = 0; i < platformInstances.size(); i++)
    {
        platformInstances[i].DrawTo(Target);
    }
    for (int i = 0; i < starList.size(); i++)
    {
        starList[i].draw(gamePointer->windowGet());
    }
    gamePointer->windowGet().draw(scoreText);
}

void LevelClass::addObstacle()
{
    if (obstacleBuffer > (1)) //Set the spawn rate to 1 per second
    {
       sf::Vector2u screensize = gamePointer->windowGet().getSize();
       int newRand = rand() % 2; //Randomise whether its a cactus or a pterodactyl
       if (newRand == 0)
       {
           Cactus newCactus(screensize);
           EnemyList.push_back(newCactus);
       }
       if (newRand == 1)
       {
           Flyer newFlyer(screensize);
           EnemyList.push_back(newFlyer);
       }
       obstacleBuffer = 0; //Reset the timer for the next spawn
       ScrollSpeed += ScrollSpeedIncrease; //Increase the speed of the game for every spawn
       Score += 5; //Increase the score for each item spawned
    }
}


