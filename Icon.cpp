#include "Icon.h"


Icon::Icon(sf::Texture& newTexture, sf::Vector2f newPosition)
	:SpriteObject(newTexture)
{
	sprite.setPosition(newPosition);
}
