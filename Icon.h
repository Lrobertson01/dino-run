#pragma once
#include "SpriteObject.h"
#include <map>
class Icon :
    public SpriteObject
{

public:
    Icon(sf::Texture& newTexture, sf::Vector2f newPosition);

};