#pragma once
#include <SFML/Graphics.hpp>
#include <SFML/Audio.hpp>
#include <map>

	class AssetManager
	{
	private:
		static std::map<std::string, sf::Texture> textures;
		static std::map<std::string, sf::SoundBuffer> sounds;
		static std::map<std::string, sf::Font> fonts;
	public:
		static sf::Texture& RequestTexture(std::string fileName);
		static sf::SoundBuffer& RequestSound(std::string fileName);
		static sf::Font& RequestFont(std::string fileName);

	};


