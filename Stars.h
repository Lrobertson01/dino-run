#pragma once
#include <SFML/Graphics.hpp>
#include <vector>

class Stars
{
public:
	Stars(sf::Texture& startexture, sf::Vector2u screensize);
	void reset();
	void update(sf::Time frameTime);
	void draw(sf::RenderWindow& gameWindow);

private:
	float speed;
	sf::Sprite Sprite;
	sf::Vector2u screenBounds;
};

