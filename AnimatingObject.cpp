#include "AnimatingObject.h"

AnimatingObject::AnimatingObject(sf::Texture& newTexture, int newFrameWidth, int newFrameHeight, float newFPS)
	: SpriteObject(newTexture)
	, frameWidth(newFrameWidth)
	, frameHeight(newFrameHeight)
	, FPS(newFPS)
	, currentFrame(0)
	, timeinFrame(sf::seconds(0.0f))
	, Clips()
	, currentClip("")
	, playing(false)
	,Looping(false)

{
}

void AnimatingObject::Update(sf::Time GameTime)
{
	if (!playing)
	{
		return;
	}
	// TODO: If it is time for a new frame...
	timeinFrame += GameTime;
	sf::Time timerPerFrame = sf::seconds(1.0f / FPS);
	if (timeinFrame >= timerPerFrame)
	{
		// Update the current frame
		++currentFrame;
		timeinFrame = sf::seconds(0);
		// If we got to the end of the clip...
		Clip& thisClip = Clips[currentClip];
		if (currentFrame > thisClip.endFrame)
		{

			// Return to the beginning of the clip
			currentFrame = thisClip.startFrame;			
			
			if (!Looping)
			{
				playing = false;
			}
		}
		UpdateSpriteTextureRect();

	}



}

void AnimatingObject::addClip(std::string name, int StartFrame, int EndFrame)
{
	Clip& newClip = Clips[name];
	// Setup the settings for this animation
	newClip.startFrame = StartFrame;
	newClip.endFrame = EndFrame;
}

void AnimatingObject::PlayClip(std::string name, bool shouldLoop)
{
	auto clipFound = Clips.find(name);

	if (clipFound != Clips.end() && currentClip != name) //Check to see if we're starting the same clip that is already playing
	{
		currentClip = name;
		currentFrame = clipFound->second.startFrame;
		timeinFrame = sf::seconds(0.0f);
		playing = true;
		Looping = shouldLoop;
		UpdateSpriteTextureRect();
	}

	if (clipFound != Clips.end() && currentClip == name)  //If the player stops moving, then starts again in the same direction then we continue the clip
	{
		Resume();
	}

}

void AnimatingObject::Pause()
{
	playing = false;
}

void AnimatingObject::Stop()
{
	playing = false;

	Clip& thisClip = Clips[currentClip];
	currentFrame = thisClip.startFrame;
}

void AnimatingObject::Resume()
{
	playing = true;
}

void AnimatingObject::UpdateSpriteTextureRect()
{
	int numFramesX = sprite.getTexture()->getSize().x / frameWidth;
	int xFrameIndex = currentFrame % numFramesX;
	int yFrameIndex = currentFrame / numFramesX;
	sf::IntRect textureRect;
	textureRect.left = xFrameIndex * frameWidth;
	textureRect.top = yFrameIndex * frameHeight;
	textureRect.width = frameWidth;
	textureRect.height = frameHeight;
	sprite.setTextureRect(textureRect);
}
