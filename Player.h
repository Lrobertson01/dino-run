#pragma once
#include "AnimatingObject.h"
#include <SFML/Audio.hpp>
class Player :
    public AnimatingObject
{
public:
    Player(sf::Vector2u Screensize);
    void Input(sf::Sound jumpSound);
    void Update(sf::Time FrameTime);
    bool checkCollision(sf::FloatRect otherHitbox);
    void setColliding(bool Collision);
    void setAlive(bool newAlive);
    bool getAlive();
    sf::FloatRect getHitbox(); //Overwrites the spriteobject Version to give us a smaller version when crouching
    
private:
    sf::Vector2f Velocity;
    float speed;
    float gravity;
    sf::Vector2f previousPos;
    const float jumpValue;
    bool isColliding;
    bool isAlive;
    bool isCrouching;
    sf::Clock changeTime;
    

};
