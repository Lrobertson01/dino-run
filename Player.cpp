#include "Player.h"
#include "AssetManager.h"

Player::Player(sf::Vector2u screensize)
	:AnimatingObject(AssetManager::RequestTexture("Assets/DinoSprite.png"), 100, 80, 8.0f)
	, Velocity(0.0f, 0.0f)
	, speed(300.0f)
	, gravity(1000.0f)
	, jumpValue(-500)
	, isColliding(false)
	, isAlive(true)
	, isCrouching(false)

	, changeTime()
{
	sf::Vector2f newPosition;
	newPosition.x = ((float)screensize.x - sprite.getGlobalBounds().width) / 8.0f;
	newPosition.y = ((float)screensize.y - sprite.getGlobalBounds().height) / 1.5f;
	sprite.setPosition(newPosition);

	addClip("Run", 0, 1);
	addClip("Crouch", 2, 3);
	addClip("Jump", 4, 4);
	addClip("Die", 6, 6);
	addClip("Stand", 5, 5);

	PlayClip("Run", true);
}

void Player::Input(sf::Sound JumpSound)
{
	isCrouching = false;
	//Give the player a bit of momentum if they stop moving
	if (Velocity.x != 0)
	{
		if (Velocity.x > 0)
		{
			Velocity.x -= speed / 1000;
		}

		if (Velocity.x < 0)
		{
			Velocity.x += speed / 1000;
		}
	}

	if (sf::Keyboard::isKeyPressed(sf::Keyboard::A) || sf::Keyboard::isKeyPressed(sf::Keyboard::Left))
	{
		Velocity.x = -speed;
	}

	if (sf::Keyboard::isKeyPressed(sf::Keyboard::D) || sf::Keyboard::isKeyPressed(sf::Keyboard::Right))
	{
		Velocity.x = speed;
	}

	if (sf::Keyboard::isKeyPressed(sf::Keyboard::W) || sf::Keyboard::isKeyPressed(sf::Keyboard::Up))
	{
		//The player can only jump if their feet are on the ground, no air infinite jump allowed
		if (isColliding == true)
		{
			sprite.setPosition(sprite.getPosition().x, sprite.getPosition().y);
			Velocity.y = jumpValue;
			JumpSound.play();
		}
	}



	if (sf::Keyboard::isKeyPressed(sf::Keyboard::S) || sf::Keyboard::isKeyPressed(sf::Keyboard::Down))
	{
		PlayClip("Crouch", true);
		isCrouching = true;
	}

	//If the player isnt on the floor they're jumping, this is more important than playing run, and so is above it
	else if (!isColliding)
	{
		PlayClip("Jump", false);
	}

	else
	{
		//Set the default animation to run, will play if no other animation plays before it 
		PlayClip("Run", true);
	}
}

void Player::Update(sf::Time FrameTime)
{
	
	previousPos = sprite.getPosition();
	
	sf::Vector2f newPosition = sprite.getPosition() + Velocity * FrameTime.asSeconds();

	sprite.setPosition(newPosition);

	AnimatingObject::Update(FrameTime);

	Velocity.y += gravity * FrameTime.asSeconds();

	//If the player is touching the "ground" then their vertical velocity cannot go below 0, meaning they cant fall through the map
	if (isColliding == true)
	{
		if (Velocity.y >= 0)
		{
			Velocity.y = 0;
		}
	}
	
}

bool Player::checkCollision(sf::FloatRect otherHitbox)
{
	if (getHitbox().intersects(otherHitbox))
	{
		return true;
	}
}

void Player::setColliding(bool Collision)
{
	isColliding = Collision;
}

void Player::setAlive(bool newAlive)
{
	isAlive = newAlive;
}

bool Player::getAlive()
{
	return isAlive;
}

sf::FloatRect Player::getHitbox()
{
	sf::FloatRect oldHitbox = SpriteObject::getHitbox();

	sf::FloatRect newHitbox = oldHitbox;
	if (isCrouching)
	{
		newHitbox.height = newHitbox.height / 2;
		newHitbox.top += oldHitbox.height * 1/2;
	}

	return newHitbox;

}

