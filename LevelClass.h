#pragma once
#include <SFML/Graphics.hpp>
#include <SFML/Audio.hpp>
#include "Player.h"
#include "Platform.h"
#include "Cactus.h"
#include "Flyer.h"
#include "Icon.h"
#include "Stars.h"
class Game;
class LevelClass
{
public:

	LevelClass(Game* newGamePointer);
	void input();
	void update(sf::Time gameTime);
	void drawTo(sf::RenderTarget& Target);
private:
	Player playerInstance;
	std::vector<Enemy> EnemyList;
	std::vector<Platform> platformInstances; //A vector of platforms to function as a floor to stand on
	std::vector<Stars> starList;
	Game* gamePointer;
	sf::View camera;
	void addObstacle();
	float ScrollSpeed;
	float ScrollSpeedIncrease;
	float obstacleBuffer;
	float Score;
	sf::Text scoreText;
	sf::Text gameOverText;
	sf::Font newFont; //The font needs to be a readily accessible variable, i found out the hard way if you make it and assign it locally things break upon printing

	sf::Sound JumpSound;
	

	sf::Music BGM;

	sf::Sound DeathSound;
	
	Icon gameOverIcon;
};

