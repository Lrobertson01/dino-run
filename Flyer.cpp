#include "Flyer.h"

Flyer::Flyer(sf::Vector2u Screensize)
	:Enemy("Assets/FlyerSprite.png", 92, 80, 4)
{
	sf::Vector2f newPosition;
	//This position allows the Pterodactyl to spawn on the far side of the screen, at a height that will mean the player gets hit if they are standing, but not if they duck
	newPosition.x = ((float)Screensize.x - sprite.getGlobalBounds().width);
	newPosition.y = ((float)Screensize.y /1.75f - sprite.getGlobalBounds().height) -25;
	sprite.setPosition(newPosition);

	addClip("Run", 0, 1);
	PlayClip("Run", true);
}
