#include "Enemy.h"
#include "AssetManager.h"



Enemy::Enemy(std::string Spritename, int SpriteSizeX, int SpriteSizeY, float FPS)
    :AnimatingObject(AssetManager::RequestTexture(Spritename), SpriteSizeX, SpriteSizeY, FPS)
    , alive(true)
    , Velocity(sf::Vector2f(500, 0))
{
}

void Enemy::update(sf::Time FrameTime, float speedMod)
{
    sf::Vector2f Newposition = sf::Vector2f(sprite.getPosition() - ((Velocity * speedMod) * FrameTime.asSeconds()));

    sprite.setPosition(Newposition);

    AnimatingObject::Update(FrameTime);
}

bool Enemy::getAlive()
{
    return alive;
}



void Enemy::SetAlive(bool newAlive)
{
    alive = newAlive;
}
