#pragma once
#include "AnimatingObject.h"
class Enemy :
    public AnimatingObject
{
public:
    Enemy(std::string Spritename, int SpriteSizeX, int SpriteSizeY, float FPS);
    void update(sf::Time FrameTime, float speedMod);
    bool getAlive();
    void SetAlive(bool newAlive);

private:
    bool alive; 
    sf::Vector2f Velocity; 
};

