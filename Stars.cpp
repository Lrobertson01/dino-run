#include "Stars.h"
#include <cstdlib>

Stars::Stars(sf::Texture& starTexture, sf::Vector2u screensize) //Use an initialisation list
	:Sprite(starTexture)
	, speed(200.0f)
	, screenBounds(screensize)
{
	reset();
}

void Stars::reset()
{

	Sprite.setPosition(sf::Vector2f(rand() % screenBounds.x, rand() % screenBounds.y/2));
	float scaleFactor = 1.0f / (1.0f + rand() % 2);
	Sprite.setScale(scaleFactor, scaleFactor);
}

void Stars::update(sf::Time frameTime)
{
	//set the sprites new position to be equal to the current position + the speed
	sf::Vector2f newPosition = Sprite.getPosition() + sf::Vector2f(-speed, 0) * frameTime.asSeconds();

	if (newPosition.x + Sprite.getTexture()->getSize().x < 0) //if the stars leave the screen then send them to the other side to wrap around
	{
		reset(); //Want to change the position and size of the stars, so its like a new one is coming from offscreen to help the illusion. But it for some reason only changes the scale, not position.
		newPosition.x = screenBounds.x;
	}
	Sprite.setPosition(newPosition);
}

void Stars::draw(sf::RenderWindow& gameWindow)
{
	gameWindow.draw(Sprite);
}
